// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#ifndef __FERO_H__
#define __FERO_H__

#define COLLISION_WEAPON		ECC_GameTraceChannel1
#define COLLISION_PROJECTILE	ECC_GameTraceChannel2
#define COLLISION_PIKCUP		ECC_GameTraceChannel3

#include "Engine.h"

#endif
